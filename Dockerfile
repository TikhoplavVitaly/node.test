FROM node:8

RUN mkdir -p /home/node/app
COPY app /home/node/app
WORKDIR /home/node/app

RUN npm install

EXPOSE 9000

CMD ['npm', 'start']